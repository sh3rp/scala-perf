package org.kndl.scalaperf

import org.scalatest.{FlatSpec, Matchers}
import scala.collection.mutable
import org.openjdk.jmh.annotations.{OutputTimeUnit, GenerateMicroBenchmark}
import java.util.concurrent.TimeUnit
import org.junit.Test
import org.openjdk.jmh.runner.options.{OptionsBuilder, Options}
import org.openjdk.jmh.runner.Runner

class ScalaHashMapTest {

  @GenerateMicroBenchmark
  @OutputTimeUnit(TimeUnit.SECONDS)
  def hashMapTest() {
    val hm = new mutable.HashMap[Int,String]()
    val r = 1 until 100000
    val words = r.start until r.end

    for (word <- words) {
      hm.put(word,"true")
    }
  }

  def main(args: Array[String]): Unit = {
    val opts:Options = new OptionsBuilder()
      .include(".*" + this.getClass().getSimpleName() + ".*")
      .warmupIterations(5)
      .measurementIterations(5)
      .forks(1)
      .build();
    new Runner(opts).run()
  }

}
