package org.kndl.scalaperf;

import java.util.HashMap;
import java.util.Map;

public class JavaHashMapTest {
    public static void main(String args[]) {
        Map<Integer,String> hm = new HashMap<Integer, String>();

        long now = System.currentTimeMillis();
        for(int i=0;i<1000000;i++) {
            hm.put(i,"true");
        }
        long later = System.currentTimeMillis();

        System.out.println("Java HashMap (1000000): " + (later - now) + "ms");
    }
}
