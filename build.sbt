name := "scala-perf"

version := "0.1"

organization := "org.kndl"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
    "org.openjdk.jmh" % "jmh-core" % "0.5.5",
    "junit" % "junit" % "4.11",
    "org.scalatest" %% "scalatest" % "2.1.0" % "test"
)
